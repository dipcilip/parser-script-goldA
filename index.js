import puppeteer from "puppeteer";
import fs, { access } from "fs";

async function saveParseData(result, url, region) {
  const text =
    url +
    `    Region= ${region}` +
    "\n" +
    "\n" +
    "name= " +
    result.nameElement +
    "\n" +
    "price= " +
    result.priceElement +
    "\n" +
    "oldPrice= " +
    result.oldPriceElement +
    "\n" +
    "article=" +
    result.article +
    "\n";
  await fs.promises.writeFile(`${result.nameElement}.txt`, text + "\n", {
    flag: "a",
  });
  const times = new Date().getTime();
  const screenName = `${times}_screenshot.jpg`;
  await fs.promises.writeFile(
    screenName,
    Buffer.from(result.screenshot, "base64")
  );
}

async function changeRegion(page, newRegion) {
  await (await page.waitForSelector("div[class*='location']")).click();
  const regionButtons = await page.waitForXPath(
    `//span[contains(text(),'${newRegion}')]`,
    { timeout: 2000 }
  );
  await Promise.all([
    regionButtons.hover(),
    regionButtons.click(),
  ]);
}

async function startParse(page) {
  try {
    const notFoundPage = await page.$x(`//h1[contains(text(),'упс...')]`);
    if (notFoundPage.length > 0) {
      throw new Error("Страница не найдена");
    }
  } catch (error) {
    throw error;
  }
  const nameElement = await page.$eval('h1 span[itemprop="name"]', (element) =>
    element.innerHTML.trim()
  );

  const priceElement = await page.$eval(
    'form div[itemprop="offers"]',
    (element) => element.innerText.trim()
  );

  const oldPriceElement = await page.$eval(
    'div[data-test-id]:has(div[itemscope="itemscope"]) > div:nth-of-type(2)',
    (element) => element.textContent.trim()
  );
  const articleElemnt = await page.$eval(
    'div[text="описание"] > div:nth-child(2)',
    (element) => element.textContent.trim()
  );
  articleElemnt[1];
  const regex = /\s*\d+/;
  const math = articleElemnt.match(regex);
  const article = math ? math[0] : null;

  return { nameElement, priceElement, oldPriceElement, article };
}

async function runner() {
  const url = process.argv[2];
  const region = process.argv[3];
  if (!url || !region) {
    throw Error("Не передан адрес или регион");
  }
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.setViewport({ width: 1366, height: 1224 });
  await page.goto(url);

  await changeRegion(page, region);

  const result = await startParse(page);
  await page.waitForTimeout(2500);
  result.screenshot = await page.screenshot({
    encoding: "base64",
    fullPage: true,
  });
  await saveParseData(result, url, region);

  await browser.close();
}

await runner();
